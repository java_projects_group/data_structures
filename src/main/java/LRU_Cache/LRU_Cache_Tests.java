package LRU_Cache;

import java.util.Iterator;
import java.util.LinkedHashMap;

class LRUCache<Key, Value> {
    private static final boolean SORT_BY_ACCESS = true;
    private static final float LOAD_FACTOR = 0.75F;
    private static final int INITIAL_CAPACITY = 16;

    private int capacity = 0;
    private final LinkedHashMap<Key,Value> map =
            new LinkedHashMap<Key,Value>(INITIAL_CAPACITY, LOAD_FACTOR, SORT_BY_ACCESS);

    public LRUCache(int capacity) {
        this.capacity = capacity;
    }

    public Value get(Key key) {
        return this.map.get(key);
    }

    public void put(Key key, Value value) {
        if (this.map.containsKey(key)) {
            map.remove(key);
        }
        else if (this.map.size() == this.capacity) {
            map.remove(this.map.keySet().iterator().next());
        }
        this.map.put(key, value);
    }
}

public class LRU_Cache_Tests {
    public static void main(String[] args) {

        LRUCache<Integer, Integer> cache = new LRUCache<>(4);
        System.out.println(cache.get(2));
        cache.put(1, 11);
        cache.put(2, 22);
        System.out.println(cache.get(2));
        cache.put(3, 33);
        cache.put(4, 44);
        cache.put(5, 55);
        System.out.println(cache.get(2));
        System.out.println(cache.get(1));
        cache.put(6, 66);
    }
}
